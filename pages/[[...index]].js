import Head from "next/head"
import React from "react"
import Canvas from "../components/canvas"
import NumericInputs from "../components/numericinputs"
import ShowHide from "../components/showhide"
import TextInputs from "../components/textinputs"
import { APP_NAME, DEFAULT_STATE } from "../utils/constants"
import { draw } from "../utils/utils"

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = DEFAULT_STATE
  }

  render() {
    const state = this.state
    const drawing = state.drawing
    
    return (
      <div>
        <Head>
          <title>{APP_NAME}</title>
          <meta name="description" content="A web tool for creating pixel art" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <header>
          <h1>{APP_NAME}</h1>
        </header>
        <main className="column">
          <div className="column resizable">
            <ShowHide>
              <TextInputs  onChange={value => this.setState(value)} state={() => state} />
            </ShowHide>
            <ShowHide>
              <NumericInputs onChange={value => this.setState(value)} state={() => state} />
            </ShowHide>              
          </div>
          { drawing.length > 0 ? 
            <div className="column resizable">
              <Canvas draw={draw(state)} />
            </div> :
            <></>
          }
        </main>
        <footer>          
        </footer>
      </div>
    )
  }
}
