import { render, screen } from "@testing-library/react"
import Home from "../pages/[[...index]]"
import { APP_NAME } from "../utils/constants"

describe("Home page", () => {
  // Workaround for test env
  URL.createObjectURL = _ => "foo"

  it("should contain the app name, text boxes, numeric inputs, plust show/hide indicators", () => {
    const { container } = render(<Home />)
    expect(screen.getByText(APP_NAME)).not.toBe(null)

    const textBoxes = container.querySelectorAll("textarea")
    expect(textBoxes.length).toBe(2)

    const numericInputs = container.querySelectorAll("input[type=number]")
    expect(numericInputs.length).toBe(3)

    expect(screen.getAllByText("-").length).toBeGreaterThan(0)
  })
})
