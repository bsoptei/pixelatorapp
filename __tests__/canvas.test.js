import { render, screen } from "@testing-library/react"
import Canvas from "../components/canvas"
import { EXPORT_IMAGE_TEXT } from "../utils/constants"

test("canvas should apply the draw function upon rendering, with itself as input, plus an export link", () => {
  let stuff = null

  // This is a workaround because the canvas package does not work in the CI
  HTMLCanvasElement.prototype.toDataURL = type => "stuff" 

  const testDraw = input => {stuff = input}
  const { container } = render(<Canvas draw={testDraw} />)
  expect(stuff).toBeInstanceOf(HTMLCanvasElement)
  expect(screen.getByText(EXPORT_IMAGE_TEXT)).not.toBe(null)

  const exportLink = container.querySelector("a")
  expect(exportLink.href).not.toBe(null)
})