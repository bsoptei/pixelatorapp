import { extractColors, stringToMatrix, drawPixel, clearCanvas, sizeCanvas, draw } from "../utils/utils"

test("extractColors should create a map from a multiline string where each line is a colon-separated key-vale pair=", () => {
  const check = input => expectedNumberOfEntries => expectedKeyValuePairs => {
    const result = extractColors(input)
    expect([...result.entries()].length).toBe(expectedNumberOfEntries)

    Object.entries(expectedKeyValuePairs).forEach(([key, value]) => {
      expect(result.get(key)).toBe(value)
    })
  }

  const gold = {"g": "gold"}
  const goldAndSilver = {"g": "gold", "s": "silver"}

  check("")(1)({})
  check("g: gold")(1)(gold)
  check("  g :   gold  ")(1)(gold)
  check("g: gold \n s: silver")(2)(goldAndSilver)

  check("g: gold \n s: silver : ")(2)(goldAndSilver)
  check("g: gold \n s: silver \n m")(3)(goldAndSilver)

})

test("stringToMatrix should split a string by lines, then lines by characters, resulting in a 2D array", () => {
  const check = input => expectedNumberOfLines => expectedNumberOfColumns => {
    const result = stringToMatrix(input)
    expect(result.length).toBe(expectedNumberOfLines)
    result.forEach(line => {
      expect(line.length).toBe(expectedNumberOfColumns)
    })
  }
  
  check("")(1)(0)
  check("ab")(1)(2)
  check("abc\ndef")(2)(3)
})
  
class MockContext {
  constructor() {
    this.filled = []
    this.cleared = []
    this.fillStyle = ""
  }

  fillRect(x, y, xSize, ySize) {
    this.filled.push({x: x, y: y, xSize: xSize, ySize: ySize, fillStyle: this.fillStyle})
  }

  clearRect(x, y, xSize, ySize) {
    this.cleared.push({x: x, y: y, xSize: xSize, ySize: ySize})
  }
}

class MockCanvas {
  constructor(width = 50, height = 50) {
    this.width = width
    this.height = height
    this.ctx = new MockContext()
  }

  getContext(ctxId) {
    if (ctxId === "2d") {
      return this.ctx
    }
  }
}

test("drawPixel should call the fillRect method of the provided context", () => {
  let mockContext = new MockContext()
  const testX = 3
  const testY = 5
  const testSize = 10

  drawPixel(mockContext)(testX)(testY)(testSize)
  expect(mockContext.filled.length).toBe(1)

  const drawn = mockContext.filled[0]
  expect(drawn.x).toBe(testX)
  expect(drawn.y).toBe(testY)
  expect(drawn.xSize).toBe(testSize)
  expect(drawn.ySize).toBe(testSize)
})

test("clearCanvas should clear the provided canvas via calling fillRect on its context", () => {
  const testWidth = 100
  const testHeight = 200
  let mockCanvas = new MockCanvas(testWidth, testHeight)
  clearCanvas(mockCanvas)

  let mockContext = mockCanvas.getContext("2d")
  expect(mockContext.cleared.length).toBe(1)
  
  const cleared = mockContext.cleared[0]
  expect(cleared.x).toBe(0)
  expect(cleared.y).toBe(0)
  expect(cleared.xSize).toBe(testWidth)
  expect(cleared.ySize).toBe(testHeight)
})

test("sizeCanvas should size the canvas", () => {
  let mockCanvas = new MockCanvas()
  const widthBefore = mockCanvas.width
  const heightBefore = mockCanvas.height
  const newWidth = 666
  const newHeight = 420

  expect(newWidth).not.toBe(widthBefore)
  expect(newHeight).not.toBe(heightBefore)

  sizeCanvas(mockCanvas)(newWidth)(newHeight)

  expect(mockCanvas.width).toBe(newWidth)
  expect(mockCanvas.height).toBe(newHeight)

})

test("draw should clear the canvas and draw based on the arguments provided", () => {
  let mockCanvas = new MockCanvas()
  const inputs = {
    drawing: "rb\nbg\ngr",
    colorMap: "r: red \n g: green \n b: blue",
    pixelSize: 10,
    padding: 1,
    zoom: 100
  }

  draw(inputs)(mockCanvas)

  const expectedFilled = [
    {
      fillStyle: "red",
      x: 0,
      xSize: 10,
      y: 0,
      ySize: 10,
    },
    {
      fillStyle: "blue",
      x: 11,
      xSize: 10,
      y: 0,
      ySize: 10,
    },
    {
      fillStyle: "blue",
      x: 0,
      xSize: 10,
      y: 11,
      ySize: 10,
    },
    {
      fillStyle: "green",
      x: 11,
      xSize: 10,
      y: 11,
      ySize: 10,
    },
    {
      fillStyle: "green",
      x: 0,
      xSize: 10,
      y: 22,
      ySize: 10,
    },
    {
      fillStyle: "red",
      x: 11,
      xSize: 10,
      y: 22,
      ySize: 10,
    }
  ]
    
  let mockContext = mockCanvas.getContext("2d")
  expect(mockContext.cleared.length).toBe(1)
  expect(mockContext.filled).toStrictEqual(expectedFilled)
})