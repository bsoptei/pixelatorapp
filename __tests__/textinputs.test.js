import { fireEvent, render } from "@testing-library/react"
import { act } from "react-dom/test-utils"
import TextInputs from "../components/textinputs"
import { DRAWING_AREA, COLOR_MAP } from "../utils/constants"

describe("TextInputs", () => {
  // Workaround for test env
  URL.createObjectURL = _ => "foo"
  
  it("should update the provided state with the label drawing and the text entered in the drawing area", () => {
    let testState = { drawing: "meh" }
    const { container } = render(<TextInputs onChange={value => testState = value} state={() => testState} />)
  
    act(() => {
      const drawingArea = container.querySelector(`#${DRAWING_AREA.id}`)
      const newValue = "haha"
      
      expect(drawingArea.value).toBe(testState.drawing)

      fireEvent.change(drawingArea, { target: { value: newValue }})
      expect(testState).toStrictEqual({drawing: newValue})
    })
  })
  
  it("should update the provided state with the label colorMap and the text entered in the color map", () => {
    let testState = { colorMap: "r: red" }
    const { container } = render(<TextInputs onChange={value => testState = value} state={() => testState} />)
  
    act(() => {
      const colorMap = container.querySelector(`#${COLOR_MAP.id}`)
      const newValue = "b: blue"

      expect(colorMap.value).toBe(testState.colorMap)
      
      fireEvent.change(colorMap, { target: { value: newValue }})
      expect(testState).toStrictEqual({colorMap: newValue})
    })
  })

  it("should have input fields to open files", async () => {
    let testState = {}
    const { container } = render(<TextInputs onChange={value => testState = value} state={() => testState} />)
    const fileInputs = container.querySelectorAll("input[type=file]")
    
    expect(fileInputs.length).toBe(2)
  })

  // export links don't get rendered in test environment, need to fix this
  it.skip("should have links to export files", async () => {
    let testState = {}
    const { container } = render(<TextInputs onChange={value => testState = value} state={() => testState} />)
    const fileExports = container.querySelectorAll("a")
    
    expect(fileExports.length).toBe(2)
  })
})
