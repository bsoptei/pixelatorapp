import { render, screen } from "@testing-library/react"
import ExportLink from "../components/exportlink"

describe("ExportLink", () => {
  // Workaround for test env
  URL.createObjectURL = stuff => stuff
  const testContent = "foobar"
  const testDescription = "bazgarply"

  it("should include a link for exporting text content to file", () => {
    const { container } = render(<ExportLink content={testContent} description={testDescription} />)
    const link = container.querySelector("a")

    expect(link.href).toMatch(/Blob/)
    expect(link.download).toBe(`${testDescription}.txt`)
  })
})
