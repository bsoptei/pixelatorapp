import { fireEvent, render } from "@testing-library/react"
import { act } from "react-dom/test-utils"
import NumericInputs from "../components/numericinputs"
import { PIXEL_SIZE, PADDING, ZOOM } from "../utils/constants"

const testState = () => {
  return { 
    pixelSize: 5,
    padding: 0,
    zoom: 3
  }
}

describe("NumericInputs", () => {
  it("should update the provided state with the label pixelSize and number provided in the input field", () => {
    let testState1 = testState()
    const { container } = render(<NumericInputs onChange={value => testState1 = value} state={() => testState1} />)
  
    act(() => {
      const pixelSizeInput = container.querySelector(`#${PIXEL_SIZE.id}`)
      const newValue = 4

      expect(Number(pixelSizeInput.value)).toBe(testState1.pixelSize)

      fireEvent.change(pixelSizeInput, { target: { value: "gibberish" }})
      expect(testState1).toStrictEqual(testState())
      
      fireEvent.change(pixelSizeInput, { target: { value: newValue }})
      expect(testState1).toStrictEqual({pixelSize: newValue})
    })
  })

  it("should update the provided state with the label padding and number provided in the input field", () => {
    let testState2 = testState()
    const { container } = render(<NumericInputs onChange={value => testState2 = value} state={() => testState2} />)
  
    act(() => {
      const paddingInput = container.querySelector(`#${PADDING.id}`)
      const newValue = 3

      expect(Number(paddingInput.value)).toBe(testState2.padding)

      fireEvent.change(paddingInput, { target: { value: "gibberish" }})
      expect(testState2.padding).toBe(0)
      
      fireEvent.change(paddingInput, { target: { value: newValue }})
      expect(testState2).toStrictEqual({padding: newValue})
    })
  })

  it("should update the provided state with the label zoom and number provided in the input field", () => {
    let testState3 = testState()
    const { container } = render(<NumericInputs onChange={value => testState3 = value} state={() => testState3} />)
  
    act(() => {
      const zoomInput = container.querySelector(`#${ZOOM.id}`)
      const newValue = 2

      expect(Number(zoomInput.value)).toBe(testState3.zoom)

      fireEvent.change(zoomInput, { target: { value: "gibberish" }})
      expect(testState3.zoom).toBe(0)
      
      fireEvent.change(zoomInput, { target: { value: newValue }})
      expect(testState3).toStrictEqual({zoom: newValue})
    })
  })
})
