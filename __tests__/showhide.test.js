import { render } from "@testing-library/react"
import { act } from "react-dom/test-utils"
import ShowHide from "../components/showhide"

describe("ShowHide", () => {
  it("should show/hide children based on its state, which can be changed via clicking on p (which shows +/- accordingly)", () => {
    const { container } = render(<ShowHide><h1>Hi!</h1></ShowHide>)
    const click = new MouseEvent("click", { bubbles: true })

    const indicator = container.querySelector("p")
    const renderedChildBefore = container.querySelector("h1")
    expect(indicator.innerHTML).toBe("-")
    expect(renderedChildBefore).not.toBe(null)

    act(() => {
      indicator.dispatchEvent(click) 
    })

    const renderedChildAfter = container.querySelector("h1")
    expect(indicator.innerHTML).toBe("+")
    expect(renderedChildAfter).toBe(null)

    act(() => {
      indicator.dispatchEvent(click) 
    })

    const renderedChildRestored = container.querySelector("h1")
    expect(indicator.innerHTML).toBe("-")
    expect(renderedChildRestored).not.toBe(null)
  })
})
