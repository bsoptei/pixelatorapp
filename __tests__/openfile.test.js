import { fireEvent, render } from "@testing-library/react"
import { act } from "react-dom/test-utils"
import OpenFile from "../components/openfile"
import { MAX_FILE_SIZE  } from "../utils/constants"
import { openFileErrorMessage } from "../utils/utils"

let testErrorMessage
let testContent
let input

beforeEach(() => {
  testErrorMessage = ""
  testContent = ""
  const { container } = render(<OpenFile 
    onChange={content => {testContent = content}} 
    handleError={error => {testErrorMessage = error}} 
    id={"test"} 
  />)

  input = container.querySelector("input")
})

const testFileContents = "foo"
const fileName = "bar.txt"
const expectedErrorMessage = openFileErrorMessage(fileName)

test("should open a valid file", async () => {
  const testFile = new File([testFileContents], fileName, { type: "text/plain" })
  testFile.text = async () => testFileContents

  await act(async () => {
    fireEvent.change(input, { target: { files: [testFile] }})
  })

  expect(testContent).toBe(testFileContents)
})

test("should report an error when the file type is not text", async () => {
  const testFileWrongType = new File([testFileContents], fileName, { type: "image/png" })

  await act(async () => {
    fireEvent.change(input, { target: { files: [testFileWrongType] }})
  })

  expect(testErrorMessage).toBe(expectedErrorMessage)
})

test("should report an error when the file size is too large", async () => {
  const tooLargeContents = "!".repeat(MAX_FILE_SIZE + 1)
  const testFileTooLarge = new File([tooLargeContents], fileName, { type: "text/plain" })

  await act(async () => {
    fireEvent.change(input, { target: { files: [testFileTooLarge] }})
  })

  expect(testErrorMessage).toBe(expectedErrorMessage)
})
