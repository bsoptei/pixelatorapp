export const extractColors = rawMap => new Map(rawMap.trim().split("\n").map(line => line.split(":").map(elem => elem.trim())))
export const stringToMatrix = s => s.split("\n").map(line => line.split(""))
export const drawPixel = context => x => y => pixelSize => context.fillRect(x, y, pixelSize, pixelSize)
export const clearCanvas = canvas => canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height)
export const sizeCanvas = canvas => width => height => {
  canvas.width = width
  canvas.height = height
}

export const draw = inputs => canvas => {
  const { drawing, colorMap, pixelSize, padding, zoom, ...rest } = inputs

  let context = canvas.getContext("2d")
  let width = 0
  let  height = 0
  let tasks = []
  const zoomPercent = zoom / 100
  const actualSize = zoomPercent * (pixelSize + padding)
  const colorMapProcessed = extractColors(colorMap)

  clearCanvas(canvas)

  stringToMatrix(drawing).forEach((row, y) => {
    let currentWidth = 0
    row.forEach((pixel, x) => {
      tasks.push(
        () => {
          let currentColor = colorMapProcessed.get(pixel)
          if (currentColor !==  undefined) {
            context.fillStyle = currentColor
            drawPixel(context)(x * actualSize)(y * actualSize)(zoomPercent * pixelSize)
          }
      })
      currentWidth += 1
    })
    if (currentWidth > width) {
      width = currentWidth
    }
    height += 1
  })
  sizeCanvas(canvas)(width * actualSize)(height * actualSize)
  tasks.forEach(task => task())
}

export const openFileErrorMessage = fileName => `Cannot open ${fileName} because it's either too large ot its format is unexpected.`
