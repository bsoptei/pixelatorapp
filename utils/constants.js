export const DEFAULT_COLOR_MAP = 
`B: black
b: blue
*: brown
c: cyan
G: gray
g: green
l: lime 
m: magenta
n: navy
o: orange
P: purple
p: pink
r: red
w: white
x: #ab67f3
y: yellow
`
export const APP_NAME = "Pixelator"
export const DEFAULT_EXPORT_FILE_NAME = "PixelatorExport.png"
export const EXPORT_IMAGE_TEXT = "Export image as png"
export const MAX_FILE_SIZE = 2_000_000

export const DEFAULT_STATE = {
  drawing: "",
  colorMap: DEFAULT_COLOR_MAP,
  pixelSize: 10,
  padding: 1,
  zoom: 100
}

export const DRAWING_AREA = {
  id: "drawingArea",
  description: "Drawing area",
  getValue: state => state.drawing,
  setValue: newValue => { return {drawing: newValue} }
}
  
export const COLOR_MAP = {
  id: "colorMap",
  description: "Color map",
  getValue: state => state.colorMap,
  setValue: newValue => { return {colorMap: newValue} }
}

export const PIXEL_SIZE = {
  id: "pixelSize",
  description: "Pixel size",
  min: 1,
  step: 1,
  getValue: state => state.pixelSize,
  setValue: newValue => { return {pixelSize: newValue} }
}

export const PADDING = {
  id: "padding",
  description: "Padding",
  min: 0,
  step: 1,
  getValue: state => state.padding,
  setValue: newValue => { return {padding: newValue} }
}

export const ZOOM = {
  id: "zoom",
  description: "Zoom",
  min: 0,
  step: 1,
  getValue: state => state.zoom,
  setValue: newValue => { return {zoom: newValue} }
}
