import { DRAWING_AREA, COLOR_MAP } from "../utils/constants"
import dynamic from "next/dynamic"
import OpenFile from "./openfile"

const TextInputs = props => {
  const { onChange, state } = props 
  const DynamicExportLink = dynamic(() => import("../components/exportlink"), { ssr: false })
  
  return [DRAWING_AREA, COLOR_MAP].map(field => {
    const fieldId = field.id
    const content = field.getValue(state())
    const description = field.description    

    return (
      <div key={`${fieldId}div`} className="column">
        <label htmlFor={fieldId}>{description}</label>
        <textarea 
          id={fieldId} 
          value={content} 
          onChange={(event) => onChange(field.setValue(event.target.value))}
        >
        </textarea>
        <div className="row">
          <DynamicExportLink content={content} description={description} /> 
          <OpenFile 
            onChange={(content) => onChange(field.setValue(content))} 
            handleError={error => console.error(error)} // there should be a better way to report errors
            id={`${fieldId}_open`} 
          />
        </div>
      </div>
    )
  })
}

export default TextInputs
