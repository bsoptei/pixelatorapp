import { useState } from "react"
import styles from "../styles/ShowHide.module.css"

function ShowHide({children}) {
  const [show, setShow] = useState(true)
  
  const [sign, childrenElement] = show ? ["-", children] : ["+", <></>]
  return <div><p className={styles.showHide} onClick={() => setShow(!show)}>{sign}</p>{childrenElement}</div>
}

export default ShowHide
