import { useRef} from "react"
import { MAX_FILE_SIZE  } from "../utils/constants"
import { openFileErrorMessage } from "../utils/utils"
import styles from "../styles/ButtonLike.module.css"

const OpenFile = props => {
  const fileRef = useRef(null)
  const { onChange, handleError, id, ...rest } = props

  return (
    <div>
      <label htmlFor={id} className={styles.buttonLike}>Import from file</label>
      <input type="file" accept=".txt" id={id} ref={fileRef} onChange={async () => {
        const file = fileRef.current.files[0]
        
        if (file.size < MAX_FILE_SIZE && file.type === "text/plain") {
          onChange(await file?.text())
        } else {
          handleError(openFileErrorMessage(file.name))
        } 
      }} />
    </div>    
  )
}

export default OpenFile
