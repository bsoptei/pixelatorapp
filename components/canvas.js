import { useEffect, useRef, useState } from "react"
import { DEFAULT_EXPORT_FILE_NAME, EXPORT_IMAGE_TEXT } from "../utils/constants"
import styles from "../styles/ButtonLike.module.css"

const Canvas = props => { 
  const canvasRef = useRef(null)
  const { draw, ...rest } = props
  const [dataUrl, setDataUrl] = useState(null)

  useEffect(() => {
    const currentCanvas = canvasRef.current

    draw(currentCanvas)
    setDataUrl(currentCanvas.toDataURL("image/png").replace("image/png", "image/octet-stream"))
  }, [draw])

  return (
    <div>
      <div className="export">
        {dataUrl === null ? <></> : (
          <a href={dataUrl} download={DEFAULT_EXPORT_FILE_NAME} className={styles.buttonLike}>{EXPORT_IMAGE_TEXT}</a>
        )}
      </div>
      <div>
        <canvas ref={canvasRef} />
      </div>
    </div>
  )
}

export default Canvas
