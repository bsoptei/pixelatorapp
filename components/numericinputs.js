import { PIXEL_SIZE, PADDING, ZOOM } from "../utils/constants"

const NumericInputs = props => {
  const { onChange, state }  = props 
  
  return [PIXEL_SIZE, PADDING, ZOOM].map(field => {
    const fieldId = field.id
    return (
      <div key={`${fieldId}div`}>
        <label htmlFor={fieldId}>{field.description}</label>
        <input 
          type="number" 
          id={fieldId} 
          name={fieldId} 
          min={field.min}
          step={field.step}
          value={field.getValue(state())} 
          onChange={(event) => {
            const newValue = Number(event.target.value)
            if (typeof newValue == "number" && newValue >= field.min) {
              onChange(field.setValue(newValue))
            }
          }}
        >
        </input>
      </div>
    )
  })
}

export default NumericInputs
