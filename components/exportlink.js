import styles from "../styles/ButtonLike.module.css"

const ExportLink = props => {
  const { content, description, ...rest } = props 
  const blob = new Blob([ content ], { type: "text/plain" })
  return (
    <div>
      <a download={`${description}.txt`} href={URL.createObjectURL(blob)} className={styles.buttonLike}>{"Export to file"}</a>  
    </div>
  )
}

export default ExportLink
